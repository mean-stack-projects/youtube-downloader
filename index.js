const express = require('express');
const app = express();
const ytdl = require('ytdl-core');
const cors = require('cors');
const config = require('config');
const bodyParser = require('body-parser');
const path = require('path')
const moment = require('moment')
const port = process.env.PORT || config.get("PORT")
//const streamToBlob = require('stream-to-blob')

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set('view engine', 'ejs');

app.use(express.static(path.resolve(__dirname, "public")))

app.get('/', (req, res) => {
    res.render('index', { page: 'Descargar videos', menuId: 'home' });
})
app.get('/success', (req, res) => {
    res.render('success', { page: 'Resultado de descarga', menuId: 'result' });
})

app.get('/download', (req, res) => {
    if(!req.query.URL){
        return res.status(400).send({"err_message": "Debe ingresar la URL"})
    }
    if(!req.query.format){
        return res.status(400).send({"err_message": "Debe especificar el formato"})
    }

    var options = {
        format: req.query.format
    }

    /*if(req.query.quality){
        options["quality"] = req.query.quality
    }*/
    /*if(req.query.fromTime || req.query.toTime){
        options["range"] = {start: req.query.fromTime || 0, end: req.query.toTime || req.query.seconds}
    }*/

    var video = ytdl(req.query.URL, {
        options
    })

    video.pipe(res)
})

function getMax(arr, prop) {
    var max;
    for (var i=0 ; i<arr.length ; i++) {
        if (max == null || parseInt(arr[i][prop]) > parseInt(max[prop]))
            max = arr[i];
    }
    return max;
}

app.get('/videoInfo', async (req, res) => {
    try {
        var info = await ytdl.getInfo(req.query.URL)
        
        let response = {
            "thumbnail": getMax(info.player_response.videoDetails.thumbnail.thumbnails, "width").url, 
            "title": info.title,
            "length_seconds": moment().startOf('day').seconds(info.length_seconds).format('H:mm:ss'),
            "seconds": info.length_seconds
        }
        res.send(response)
    } catch (error) {
        console.log("Error al obtener información del video", error)
    }
    
    

})

app.listen(port, () => {
    console.log(`App listen on port ${config.get("PORT")}`)
})