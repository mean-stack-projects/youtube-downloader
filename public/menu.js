function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

function isAudioFormat(format) {
    return format == 'mp3' || format == 'ogg'
}

$(document).ready(() => {
    var element = $('meta[name="active-menu"]').attr('content');

    $('#' + element).addClass('active');

    $('#fromTime').mask('00:00:00')
    $('#toTime').mask('00:00:00')

    $("#videoURL").change(function (ev) {
        if (isUrlValid(ev.target.value)) {
            $('.loadingScreen').show()
            fetch('/videoInfo?URL=' + ev.target.value, { method: "GET" })
                .then(response => response.json())
                .then(body => {
                    $('.loadingScreen').hide()
                    $('#videoTitle').text(body.title)
                    $('#videoLength').text(body.length_seconds)
                    $('#videoThumbnail').attr("src", body.thumbnail)
                    $('.hiddenUntilVideoInfo').show("slow")
                    $('#btnDescargar').attr("disabled", false)
                })
        }
    });

    $('#partialDownload').change(function (ev) {
        if (ev.target.checked) {
            $('.partialDownloadParams').show()
        } else {
            $('.partialDownloadParams').hide()
        }
    })

    $('#qualitySelect').change(function (ev) {

    })

    var convertBtn = document.querySelector('#btnDescargar');
    var URLinput = document.querySelector('#videoURL');

    convertBtn.addEventListener('click', (ev) => {
        console.log(`URL: ${URLinput.value}`);
        sendURL(URLinput.value, $("#format").val());
    });
    function sendURL(URL, format) {
        $('.loadingScreen').show()
        console.log($('#format').val())
        fetch('/download?URL=' + URL + "&format=" + format)
            .then(response => response.blob())
            .then(blob => {
                var url = window.URL.createObjectURL(blob);
                var a = document.createElement('a');
                a.href = url;
                a.download = $('#videoTitle').text() + '.' + $('#format').val();
                document.body.appendChild(a);
                a.click();
                a.remove();
                $('.loadingScreen').hide()
            });
        //window.location.href = `http://localhost:3001/download?URL=${URL}`;
    }
});

